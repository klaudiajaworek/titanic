import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv") 
    return df


def get_filled():
    df = get_titatic_dataframe()
    df = df.assign(Prefix = [name[(name.find(",") + 2):(name.find(".") + 1)] for name in df["Name"]])
    age_median = df.groupby("Prefix")["Age"].median().round(0).astype("int")
    missing_number_mr = df.loc[df["Prefix"] == "Mr.", "Age"].isna().sum()
    missing_number_mrs = df.loc[df["Prefix"] == "Mrs.", "Age"].isna().sum()
    missing_number_miss = df.loc[df["Prefix"] == "Miss.", "Age"].isna().sum()
    return [("Mr.", missing_number_mr, age_median["Mr."]), 
            ("Mrs.", missing_number_mrs, age_median["Mrs."]),
            ("Miss.", missing_number_miss, age_median["Miss."])]
